﻿namespace Bastion.Authorization
{
    using System.Collections.Generic;

    using Microsoft.AspNetCore.Identity;

    /// <summary>
    /// Bastion user
    /// </summary>
    public class BastionUser : IdentityUser
    {
        /// <summary>
        /// Devices.
        /// </summary>
        public virtual ICollection<BastionUserDevice> Devices { get; set; } = new HashSet<BastionUserDevice>();

        /// <summary>
        /// Lockout new device.
        /// </summary>
        public bool LockoutNewDevice { get; set; }

        /// <summary>
        /// Two-factor primary provider.
        /// </summary>
        public string TwoFactorPrimary { get; set; }
    }
}
