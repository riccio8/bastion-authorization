﻿namespace Bastion.Authorization
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Bastion user device.
    /// </summary>
    public class BastionUserDevice : IEquatable<BastionUserDevice>
    {
        /// <summary>
        /// Id.
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        /// <summary>
        /// User id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// User.
        /// </summary>
        public virtual BastionUser User { get; set; }

        /// <summary>
        /// Enabled.
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// History.
        /// </summary>
        public virtual ICollection<BastionUserDeviceHistory> History { get; set; } = new HashSet<BastionUserDeviceHistory>();

        /// <summary>
        /// Ts.
        /// </summary>
        public DateTime Ts { get; set; }

        /// <summary>
        /// Ip.
        /// </summary>
        public string Ip { get; set; }

        /// <summary>
        /// Browser.
        /// </summary>
        public string Browser { get; set; }

        /// <summary>
        /// City.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Country.
        /// </summary>
        public string Country { get; set; }

        public bool Equals(BastionUserDevice device)
        {
            return Ip == device.Ip && City == device.City && Country == device.Country && Browser == device.Browser;
        }
    }
}
