﻿namespace Bastion.Authorization
{
    public class UserModel
    {
        public UserModel()
        {

        }

        public UserModel(BastionUser user, string bearer)
        {
            Bearer = bearer;
            UserId = user.Id;
            Email = user.Email;
        }

        /// <summary>
        /// User id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// User's first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// User's last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// User's email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// User's authorization token (JWT Bearer)
        /// </summary>
        public string Bearer { get; set; }

        /// <summary>
        /// Roles
        /// </summary>
        public string[] Roles { get; set; }

        /// <summary>
        /// Profile.
        /// </summary>
        public ProfileModel Profile { get; set; }

        /// <summary>
        /// Confirmed status.
        /// </summary>
        public ConfirmedStatus ConfirmedStatus { get; set; }

        /// <summary>
        /// Is VIP.
        /// </summary>
        public bool IsVip { get; set; }
    }
}