﻿namespace Bastion.Authorization
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Confirm Email ViewModel
    /// </summary>
    public class ConfirmEmailViewModel
    {
        /// <summary>
        /// UserId
        /// </summary>
        [Required]
        public string UserId { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        [Required]
        public string Code { get; set; }
    }
}