﻿namespace Bastion.Authorization
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Confirm Email ViewModel
    /// </summary>
    public class ResetPasswordViewModel
    {
        /// <summary>
        /// User id.
        /// </summary>
        [Required]
        public string UserId { get; set; }

        /// <summary>
        /// Password.
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// Reset password code.
        /// </summary>
        [Required]
        public string Code { get; set; }
    }
}