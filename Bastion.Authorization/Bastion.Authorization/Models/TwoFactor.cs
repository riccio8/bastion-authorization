﻿namespace Bastion.Authorization
{
    public class TwoFactor
    {
        /// <summary>
        /// Two-Factor code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Two-Factor provider.
        /// </summary>
        public string Provider { get; set; }
    }
}