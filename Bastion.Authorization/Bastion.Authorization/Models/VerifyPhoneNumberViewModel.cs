﻿namespace Bastion.Authorization
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Verify phone number ViewModel.
    /// </summary>
    public class VerifyPhoneNumberViewModel : AddPhoneNumberViewModel
    {
        /// <summary>
        /// Code.
        /// </summary>
        [Required]
        public string Code { get; set; }
    }
}