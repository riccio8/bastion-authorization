﻿namespace Bastion.Authorization
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Two factor authentication chnage ViewModel.
    /// </summary>
    public class TwoFactorAuthenticationChangeViewModel
    {
        [Required]
        public bool Enable { get; set; }
    }
}