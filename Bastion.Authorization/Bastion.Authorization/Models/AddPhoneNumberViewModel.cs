﻿namespace Bastion.Authorization
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Add phone number ViewModel.
    /// </summary>
    public class AddPhoneNumberViewModel
    {
        /// <summary>
        /// Phone number.
        /// </summary>
        [Phone]
        [Required]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
    }
}