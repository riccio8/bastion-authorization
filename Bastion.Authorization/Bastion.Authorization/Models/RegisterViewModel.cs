﻿namespace Bastion.Authorization
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Register ViewModel
    /// </summary>
    public class RegisterViewModel : RegisterBase
    {
        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// CallbackUrl
        /// </summary>
        [DataType(DataType.Text)]
        [Display(Name = "CallbackUrl")]
        public string CallbackUrl { get; set; }

        public string DocumentNumber { get; set; }
        public string DocumentTaker { get; set; }
        public DateTime? DocumentDate { get; set; }
        public string AddressFact { get; set; }

        public string Company { get; set; }

        public string ClientType { get; set; }
    }
}