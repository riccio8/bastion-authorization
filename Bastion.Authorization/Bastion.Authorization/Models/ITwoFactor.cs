﻿namespace Bastion.Authorization
{
    public interface ITwoFactor
    {
        TwoFactor TwoFactor { get; set; }
    }
}