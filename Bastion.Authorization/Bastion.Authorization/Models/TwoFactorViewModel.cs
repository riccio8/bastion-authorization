﻿namespace Bastion.Authorization
{
    /// <summary>
    /// Two-Factor ViewModel
    /// </summary>
    public class TwoFactorViewModel : ITwoFactor
    {
        /// <summary>
        /// Two-Factor.
        /// </summary>
        public TwoFactor TwoFactor { get; set; }
    }
}