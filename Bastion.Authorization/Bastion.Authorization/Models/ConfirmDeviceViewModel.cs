﻿namespace Bastion.Authorization
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Confirm device view model.
    /// </summary>
    public class ConfirmDeviceViewModel
    {
        /// <summary>
        /// User id.
        /// </summary>
        [Required]
        public string UserId { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        [Required]
        public string Code { get; set; }

        /// <summary>
        /// Device id.
        /// </summary>
        [Required]
        public string DeviceId { get; set; }
    }
}