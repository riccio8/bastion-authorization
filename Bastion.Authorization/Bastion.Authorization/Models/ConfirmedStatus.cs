﻿namespace Bastion.Authorization
{
    using Bastion.Notifications.Abstractions;

    using System.Collections.Generic;

    /// <summary>
    /// Confirmed status.
    /// </summary>
    public class ConfirmedStatus : INotifiable
    {
        /// <summary>
        /// Email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Phone number.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Email confirmed.
        /// </summary>
        public bool EmailConfirmed { get; set; }

        /// <summary>
        /// Phone number confirmed.
        /// </summary>
        public bool PhoneNumberConfirmed { get; set; }

        /// <summary>
        /// Push confirmed.
        /// </summary>
        public bool PushConfirmed { get; set; } = false;

        /// <summary>
        /// Two factor enabled.
        /// </summary>
        public bool TwoFactorEnabled { get; set; }

        /// <summary>
        /// Two factor primary.
        /// </summary>
        public string TwoFactorPrimary { get; set; }

        /// <summary>
        /// Two factor providers.
        /// </summary>
        public IList<string> TwoFactorProviders { get; set; }

        /// <summary>
        /// Two factor authenticator enabled.
        /// </summary>
        public bool TwoFactorAuthenticatorEnabled { get; set; }

        public string NotificationMethod => "onConfirmedStatus";
    }
}