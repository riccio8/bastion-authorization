﻿using Bastion.Kernel;
using Microsoft.Extensions.Configuration;

namespace Bastion.Authorization
{
    public static class JWTSettings
    {
        public static string JwtKey { get; private set; }
        public static string JwtIssuer { get; private set; }
        public static double JwtExpireDays { get; private set; }

        static JWTSettings()
        {
            JwtKey = CfgKernel.Configuration.GetSection("JWT:Key").Get<string>();
            if (string.IsNullOrWhiteSpace(JwtKey))
            {
                throw new System.Exception("Jwt tokent isn't set in the config file. JWT:Key.");
            }
            JwtIssuer = CfgKernel.Configuration.GetSection("JWT:Issuer").Get<string>();
            if (string.IsNullOrWhiteSpace(JwtKey))
            {
                throw new System.Exception("Jwt issuer isn't set in the config file. JWT:Issuer.");
            }
            JwtExpireDays = CfgKernel.Configuration.GetSection("JWT:ExpireDays").Get<double>();
            if (JwtExpireDays == 0)
            {
                throw new System.Exception("Jwt expire days isn't set in the config file. JWT:ExpireDays.");
            }
        }
    }
}