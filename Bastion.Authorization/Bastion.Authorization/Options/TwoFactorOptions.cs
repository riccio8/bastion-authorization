﻿namespace Bastion.Authorization.Options
{
    using System.Text.Encodings.Web;

    /// <summary>
    /// Two factor authentication options.
    /// </summary>
    public class TwoFactorOptions
    {
        private static readonly string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits={3}";

        /// <summary>
        /// Issuer
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// Digits
        /// </summary>
        public int Digits { get; set; }

        public string ToUri(UrlEncoder urlEncoder, string email, string authenticatorKey)
        {
            return string.Format(
                AuthenticatorUriFormat,
                urlEncoder.Encode(Issuer),
                urlEncoder.Encode(email),
                authenticatorKey,
                Digits);
        }
    }
}