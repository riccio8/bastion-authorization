﻿namespace Bastion.Authorization.Options
{
    /// <summary>
    /// User default options.
    /// </summary>
    public class UserDefaultOptions
    {
        /// <summary>
        /// Lock out new device.
        /// </summary>
        public bool LockoutNewDevice { get; set; } = true;

        /// <summary>
        /// Add device at registration.
        /// </summary>
        public bool AddDeviceAtRegistration { get; set; } = true;

        /// <summary>
        /// Validate device in jwt token.
        /// </summary>
        public bool ValidateDeviceInJwtToken { get; set; } = true;
    }
}
