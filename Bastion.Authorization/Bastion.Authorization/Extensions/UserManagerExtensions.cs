﻿namespace Bastion.Authorization.Extensions
{
    using Microsoft.AspNetCore.Identity;

    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public static class UserManagerExtensions
    {
        public static async Task<ConfirmedStatus> GetConfirmedStatus<TUser>(
            this UserManager<TUser> userManager,
            TUser user)
            where TUser : BastionUser
        {
            var twoFactorProviders = await userManager.GetEnabedTwoFactorProvidersAsync(user);

            return new ConfirmedStatus
            {
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                EmailConfirmed = user.EmailConfirmed,
                TwoFactorProviders = twoFactorProviders,
                TwoFactorPrimary = user.TwoFactorPrimary,
                TwoFactorEnabled = user.TwoFactorEnabled,
                PhoneNumberConfirmed = user.PhoneNumberConfirmed,
                TwoFactorAuthenticatorEnabled = twoFactorProviders.Any(x => x == userManager.Options.Tokens.AuthenticatorTokenProvider),
            };
        }

        public static async Task<IList<string>> GetEnabedTwoFactorProvidersAsync<TUser>(
            this UserManager<TUser> userManager,
            TUser user)
            where TUser : BastionUser
        {
            var twoFactorProviders = await userManager.GetValidTwoFactorProvidersAsync(user);

            return !string.IsNullOrEmpty(user.TwoFactorPrimary) && twoFactorProviders.FirstOrDefault(x => x == user.TwoFactorPrimary) != null
                ? new string[] { user.TwoFactorPrimary }
                : twoFactorProviders?.Where(x => x != "Email")?.ToArray();
        }
    }
}
